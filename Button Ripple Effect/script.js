const buttonElement = document.querySelector(".button");

buttonElement.addEventListener("mouseover", (event) => {
    // console.log(event.pageX)
    const x = event.pageX - buttonElement.offsetLeft;
    const y = event.pageY - buttonElement.offsetTop;

    buttonElement.style.setProperty("--xpos", x + 'px');
    buttonElement.style.setProperty("--yPos", y + 'px');
})