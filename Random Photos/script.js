const imageContainerElement = document.querySelector(".image-container");

const buttonElement = document.querySelector(".button");

buttonElement.addEventListener("click", () => {
    addNewImages()
});

function addNewImages(){
    const newImageElement = document.createElement('img');
    newImageElement.src = `https://picsum.photos/200/300?random=${Math.floor(Math.random() * 2000)}`;
    imageContainerElement.appendChild(newImageElement);
}

