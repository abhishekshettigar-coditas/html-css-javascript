const number1 = Math.ceil(Math.random()*10);

const number2 = Math.ceil(Math.random()*10);

const questionElement = document.getElementById('question')

const formElement = document.getElementById('form');

const inputElement = document.getElementById('input');

let score = 0;

questionElement.innerText = `What is ${number1} multiply by ${number2} `;

const correctAns = number1*number2;

formElement.addEventListener('submit', () => {
    const userAns = +inputElement;
    if (userAns === correctAns){
        score++;
        console.log(score);
    }else{
        score--;
        console.log(score);
    }
})

// function updateLocalStorage(){
//     localStorage.setItem("score")
// }
