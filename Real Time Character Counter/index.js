const textareaElement = document.getElementById('textarea');

const totalCounterElement = document.getElementById('total-counter')
const remainingCounterElement = document.getElementById('remaining-counter')

updateCounter();

textareaElement.addEventListener('keyup', () =>{
    updateCounter()
})

function updateCounter(){
    totalCounterElement.innerText = textareaElement.value.length;
    remainingCounterElement.innerText = textareaElement.getAttribute('maxlength') - textareaElement.value.length;
}