const containerElement = document.querySelector('.container');

const careers = ['Ashish', 'Ashutosh', 'mohit', 'rohit'];

let careerIndex = 0;

let characterIndex = 0;

updateText();

 function updateText() {
     characterIndex++;
     containerElement.innerHTML = `I am  ${careers[careerIndex].slice(0, characterIndex)}`;

     if(characterIndex === careers[careerIndex].length){
        careerIndex++;
        characterIndex = 0;
     }

     if(careerIndex === careers.length){
        careerIndex = 0;
     }
     setTimeout(updateText, 400);
 }